#!/bin/bash

while read p; do	

	NAME=` basename ${p} | cut -d '-' -f 1 `

	cd derivatives/qsirecon/sub-${NAME}/ses-01/dwi/

	dwiextract sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.nii.gz - -bzero -fslgrad sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bvec sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bval | mrmath - mean sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_meanb0.nii.gz -axis 3

	bet sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_meanb0.nii.gz sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET -m -R -f 0.3

        dwi2response dhollander sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.nii.gz ${NAME}_sfwm.txt ${NAME}_gm.txt ${NAME}_csf.txt -fa 0.1 -mask sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz -fslgrad sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bvec sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bval

	rm sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_meanb0.nii.gz

	rm sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET.nii.gz

	rm sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz

	cd ../../../../../

done <$1

#responsemean derivatives/qsirecon/sub-*/ses-01/dwi/*_sfwm.txt average_sfwm.txt
#responsemean derivatives/qsirecon/sub-*/ses-01/dwi/*_csf.txt average_csf.txt
#responsemean derivatives/qsirecon/sub-*/ses-01/dwi/*_gm.txt average_gm.txt

average_response derivatives/qsirecon/sub-*/ses-01/dwi/*_sfwm.txt average_sfwm.txt
average_response derivatives/qsirecon/sub-*/ses-01/dwi/*_csf.txt average_csf.txt
average_response derivatives/qsirecon/sub-*/ses-01/dwi/*_gm.txt average_gm.txt
