#!/bin/bash

for map in FA MD L1; do
    echo "Computing mean ${map}..."
    filename="tracts_${map}.csv"
    header="record_id,AF_left,AF_left_weights,AF_left_voxels,AF_left_volume,AF_right,AF_right_weights,AF_right_voxels,AF_right_volume,ATR_left,ATR_left_weights,ATR_left_voxels,ATR_left_volume,ATR_right,ATR_right_weights,ATR_right_voxels,ATR_right_volume,CC_genu,CC_genu_weights,CC_genu_voxels,CC_genu_volume,CCG_left,CCG_left_weights,CCG_left_voxels,CCG_left_volume,CCG_right,CCG_right_weights,CCG_right_voxels,CCG_right_volume,CC_splenium,CC_splenium_weights,CC_splenium_voxels,CC_splenium_volume,CST_left,CST_left_weights,CST_left_voxels,CST_left_volume,CST_right,CST_right_weigths,CST_right_voxels,CST_right_volume,IFOF_left,IFOF_left_weights,IFOF_left_voxels,IFOF_left_volume,IFOF_right,IFOF_right_weights,IFOF_right_voxels,IFOF_right_volume,ILF_left,ILF_left_weights,ILF_left_voxels,ILF_left_volume,ILF_right,ILF_right_weights,ILF_right_voxels,ILF_right_volume,UNC_left,UNC_left_weights,UNC_left_voxels,UNC_left_volume,UNC_right,UNC_right_weights,UNC_right_voxels,UNC_right_volume"
    printf "$header\n" > $filename
    while read p; do
	NAME=` basename ${p} | cut -d '-' -f 1 `
        printf "%s," ${NAME} >> $filename
        for tract in AF_left AF_right ATR_left ATR_right CC_genu CCG_left CCG_right CC_splenium CST_left CST_right IFOF_left IFOF_right ILF_left ILF_right UNC_left UNC_right; do
            printf "%s," $(fslstats derivatives/qsirecon/sub-${NAME}/ses-01/dwi/sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_${map}_weighted.nii.gz -k derivatives/qsirecon/sub-${NAME}/ses-01/dwi/sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_mask.nii.gz -m) >> $filename
            printf "%s," $(fslstats derivatives/qsirecon/sub-${NAME}/ses-01/dwi/${tract}_tdi_norm.nii.gz -k derivatives/qsirecon/sub-${NAME}/ses-01/dwi/sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_mask.nii.gz -m) >> $filename
            printf "%s," $(fslstats derivatives/qsirecon/sub-${NAME}/ses-01/dwi/sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_mask.nii.gz -V) >> $filename
        done
        printf '\n' >> $filename
    done <$1
    sed -i 's/.$//' $filename
done
echo "Done!"
