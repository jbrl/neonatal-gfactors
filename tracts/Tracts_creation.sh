#!/bin/bash

#echo "Creating weighted ${map} tracts..."

while read p; do

NAME=` basename ${p} | cut -d '-' -f 1 `
   
cd derivatives/qsirecon/sub-${NAME}/ses-01/dwi/

dwiextract sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.nii.gz - -bzero -fslgrad sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bvec sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bval | mrmath - mean sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_meanb0.nii.gz -axis 3

bet sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_meanb0.nii.gz sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET -m -R -f 0.3

dtifit -k sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.nii.gz -r sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bvec -b sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bval -w -o sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI -m sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz

dwi2fod msmt_csd sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.nii.gz ../../../../../average_sfwm.txt sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_wmfod.mif ../../../../../average_csf.txt sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_csffod.mif -mask sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz -fslgrad sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bvec sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_dwi.bval

mtnormalise sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_wmfod.mif sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_wmfodnorm.mif sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_csffod.mif sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_csffodnorm.mif -mask sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz

antsRegistrationSyN.sh -d 3 -f ../../../../../ENA50/ENA50_FA.nii.gz -m sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI_FA.nii.gz -j 1 -o subject_2_template_

for tract in AF_left AF_right ATR_left ATR_right CC_genu CCG_left CCG_right CC_splenium CST_left CST_right IFOF_left IFOF_right ILF_left ILF_right UNC_left UNC_right; do

	antsApplyTransforms -d 3 -r sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI_FA.nii.gz -i ../../../../../ENA50/${tract}.nii.gz -t [subject_2_template_0GenericAffine.mat, 1] -t subject_2_template_1InverseWarp.nii.gz -o ${tract}.nii.gz -n NearestNeighbor -v 1

	fslmaths ${tract}.nii.gz -dilD ${tract}_dil.nii.gz

	fslmaths ${tract}.nii.gz -mul sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz ${tract}.nii.gz

	fslmaths ${tract}_dil.nii.gz -mul sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz ${tract}_dil.nii.gz

	tckgen sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_wmfodnorm.mif ${tract}.tck -minlength 20 -maxlength 250 -mask ${tract}_dil.nii.gz -seed_image ${tract}.nii.gz -seeds 200000

	tckmap ${tract}.tck ${tract}_tdi.nii.gz -template sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_wmfodnorm.mif

	fslmaths ${tract}_tdi.nii.gz -mul sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_BET_mask.nii.gz ${tract}_tdi.nii.gz

	val=( $(mrstats -ignorezero ${tract}_tdi.nii.gz -output max) )

	mrcalc ${tract}_tdi.nii.gz ${val[0]} -div ${tract}_tdi_norm.nii.gz

	for map in FA MD L1; do

		fslmaths sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI_${map}.nii.gz -mul ${tract}_tdi_norm.nii.gz sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_${map}_weighted.nii.gz

		fslmaths ${tract}_tdi_norm.nii.gz -bin sub-${NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_${tract}_mask.nii.gz

		#cp ${BASENAME}/${tract}_tdi_norm.nii.gz weighted_tracts/${BASENAME}_${tract}_weights.nii.gz

	done

done

cd ../../../../../

done <$1
