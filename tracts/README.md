**Preprocessing pipeline**

Here, we present a step by step guide about how to create the tracts. Note that this scripts make use of several tools and codes that require their corresponding citation.

**Step 1: Preprocessing of the data (optional)**

The first step is how to preprocess your data using QSIprep (if you have your data already preprocessed you can skip this step): 

`singularity run -e --nv -B {PATHS TO BIND IN YOUR SYSTEM} {QSIprep_path}/qsiprep-0.14.2.sif BIDs_data derivatives participant --participant-label {NAME} -w {NAME}_QSIprep_working_directory --nthreads 16 --omp-nthreads 16 --fs-license-file {PATH_TO_FREESURFER}/license.txt --dwi-denoise-window 5 --unringing-method mrdegibbs --dwi-only --infant --output-resolution 1.25 --eddy-config eddy_params.json --recon-spec reorient_fslstd --stop-on-first-crash --skip_bids_validation` 

This command assumes a structure like this:

```
├── BIDs_data
│   ├── dataset_description.json
│   └── sub-{NAME}
│       └── ses-01
│           ├── anat
│           │   ├── sub-{NAME}_ses-01_T1w.json
│           │   └── sub-{NAME}_ses-01_T1w.nii.gz
│           └── dwi
│               ├── sub-{NAME}_ses-01_dwi.bval
│               ├── sub-{NAME}_ses-01_dwi.bvec
│               ├── sub-{NAME}_ses-01_dwi.json
│               └── sub-{NAME}_ses-01_dwi.nii.gz
└── eddy_params.json
```


The eddy_params.json file:

```
{
  "flm": "linear",
  "slm": "linear",
  "fep": false,
  "interp": "spline",
  "nvoxhp": 1000,
  "fudge_factor": 10,
  "dont_sep_offs_move": false,
  "dont_peas": false,
  "niter": 8,
  "method": "jac",
  "repol": true,
  "num_threads": 1,
  "is_shelled": true,
  "use_cuda": true,
  "cnr_maps": true,
  "residuals": true,
  "output_type": "NIFTI_GZ",
  "args": "--fwhm=10,6,4,2,0,0,0,0 --ol_type=both --mporder=8 --s2v_niter=8 --json={PATH_to_BIDS-data}/sub-{NAME}/ses-01/dwi/sub-{NAME}_ses-01_dwi.json --ol_nstd=6"
}
```
The flag --nv makes the singularity to use GPU. if you don't have GPU you can drop it and use the following eddy_params.json:

```
{
  "flm": "linear",
  "slm": "linear",
  "fep": false,
  "interp": "spline",
  "nvoxhp": 1000,
  "fudge_factor": 10,
  "dont_sep_offs_move": false,
  "dont_peas": false,
  "niter": 8,
  "method": "jac",
  "repol": true,
  "num_threads": 1,
  "is_shelled": true,
  "use_cuda": true,
  "cnr_maps": true,
  "residuals": true,
  "output_type": "NIFTI_GZ",
  "args": "--fwhm=10,6,4,2,0,0,0,0 --ol_type=both --ol_nstd=6"
}
```

The result of this step is a `derivatives/` folder that contains all the output from QSIprep. The folder that we need for the tracts generation is `derivatives/qsirecon/sub-{NAME}/ses-01/dwi`. From now on, **we assume that you have the data in the same format**. If that's not the case, you have to edit the scripts or reorder your data.

**For the next steps to work, you need to have [MRtrix3](https://www.mrtrix.org/), [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki) and [ANTs](https://github.com/ANTsX/ANTs) installed and added to your path.**

**Step 2: Response function calculation**

This framework allows to use different models for tractography generation, in the original [manuscript](https://www.biorxiv.org/content/10.1101/2021.11.29.470344v1) Constrained spherical deconvolution was used. So the instruction here will be using that model.

Do you need to use the same response function for all your subjects, to do this you can calculet the response function for each subject (or a subset of your population) and the average it. The script `Response_function_estimation.sh` will do that for you.

`./Response_function_estimation.sh list.txt`

Where `list.txt` is a text file containing the IDs of the subjects that you want to use for the response function estimation.

```
list.txt:
id1
id2
.
.
.
idn
```
You don't have to add the preffix `sub-` in your IDs.

Depending of the version of MRtrix3 you are using, you should uncomment the 15-17 and comment the lines 19-21. The default command `average_responses` was replaced in the version 3.0 by `responsemean`.

This step will create three files in your main folder: `average_sfwm.txt`, `average_csf.txt` and `average_gm.txt`. The first two are the ones that will be used for the following step.

**Step 3: Creation of the DTI/NODDI maps and registration to the template**

First, you need to create a folder called `ENA50` in the directory you are in, and copy inside all the files for the atlas (the [ENA50 FA template](https://git.ecdf.ed.ac.uk/jbrl/ena/-/tree/master/ENA50) and the tracts from this repository). The `Tracts_creation.sh` script needs to be run from the current folder.

`./Tracts_creation.sh list.txt`

Where `list.txt` is a text file containing the IDs of the subjects that you want to analyze (the format is the same before).

This script will generate, in each subject's folder, all the required files to compute the weighted mean of each tract.

By default, the code only computes the FA, MD and L1, if you want to add other maps (NODDI, RD, etc...), you just need to add, before running the script, the corresponding map to the subject's folder (`/EBC/home/mblesa/QSIprep_phase1/derivatives/qsirecon/sub-{NAME}/ses-01/dwi`) with the following name: `sub-{NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI_{MAP}.nii.gz` (the DTI suffix has to be always there) and then edit the `Tracts_creation.sh` script and add the {MAP} to the line 43. For example to add the fintra obtained from NODDI, you sould add the fintra map to the subject folder with the name `sub-{NAME}_ses-01_space-T1w_desc-preproc_space-T1w_fslstd_DTI_fintra.nii.gz` and then edit the line 43 to be: `for map in FA MD L1 fintra; do`

If you are using multishell data, probably you want to edit the `dtifit` call (line 15).

**Step 4: Mean values extraction**

In the same folder as before, you just need to run the script `Weighted_means.sh` in the same way as the previous script:

`./Weighted_means.sh list.txt`

This will create a csv file for each map with all the values for each subject.

If you modified the line 43 in the previous step, you need to do the same in this script with the line 3.

To obtain the weighted mean you need to divide the column {tract_name} by the column {tract_name}_weights








