---
title: "`r subdir_name`"
author: "Kadi Vaher"
date: "`r Sys.time()`"
output: html_document
---

```{r setup, include=T, message=F}

rm(list = ls())
gc()
subdir_name <- "singlemetric_correlations"

# load packages
library(tidyverse)
library(magrittr)
library(here)
library(glue)
library("Hmisc")
library(corrplot)
library(DescTools)

# set paths
knitr::opts_knit$set(root.dir = ".", aliases = c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(
  dev = c("tiff"),
  fig.path = here("results", "figures", "paper_figures", glue("{subdir_name}/")), dpi = 300
)
theme_set(theme_light()) # global option for ggplot2
```


```{r knit, echo=F, eval=FALSE}

# Run this section at the very end

rmarkdown::render(
  input = here("scripts", str_c(subdir_name, ".Rmd")),
  output_file = here("results", "reports", paste(subdir_name, ".html", sep="_"))
)
```

# Function for flattening the correlation matrix

```{r flatten_function}

# ++++++++++++++++++++++++++++
# flattenCorrMatrix
# ++++++++++++++++++++++++++++
# cormat : matrix of the correlation coefficients
# pmat : matrix of the correlation p-values
flattenCorrMatrix <- function(cormat, pmat) {
  ut <- upper.tri(cormat)
  data.frame(
    row = rownames(cormat)[row(cormat)[ut]],
    column = rownames(cormat)[col(cormat)[ut]],
    cor  =(cormat)[ut],
    p = pmat[ut]
    )
}

```


# FA

```{r FA}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "FA"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


FA_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

FA_cor_flat <- FA_cor_flat %>% mutate(FishersF = FisherZ(cor))

FA_cor_flat <- FA_cor_flat %>% mutate(metric="FA")

knitr::kable(FA_cor_flat)

#mean correlation coefficient
FisherZInv(mean(FA_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(FA_cor_flat$FishersF))


```

# MD

```{r MD}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "MD"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


MD_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

MD_cor_flat <- MD_cor_flat %>% mutate(FishersF = FisherZ(cor))

MD_cor_flat <- MD_cor_flat %>% mutate(metric="MD")

knitr::kable(MD_cor_flat)

#mean correlation coefficient
FisherZInv(mean(MD_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(MD_cor_flat$FishersF))


```

# AD

```{r AD}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "L1"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


AD_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

AD_cor_flat <- AD_cor_flat %>% mutate(FishersF = FisherZ(cor))

AD_cor_flat <- AD_cor_flat %>% mutate(metric="AD")

knitr::kable(AD_cor_flat)

#mean correlation coefficient
FisherZInv(mean(AD_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(AD_cor_flat$FishersF))


```

# RD

```{r RD}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "RD"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


RD_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

RD_cor_flat <- RD_cor_flat %>% mutate(FishersF = FisherZ(cor))

RD_cor_flat <- RD_cor_flat %>% mutate(metric="RD")

knitr::kable(RD_cor_flat)

#mean correlation coefficient
FisherZInv(mean(RD_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(RD_cor_flat$FishersF))


```

# NDI

```{r NDI}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "ficvfWatWM"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


NDI_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

NDI_cor_flat <- NDI_cor_flat %>% mutate(FishersF = FisherZ(cor))

NDI_cor_flat <- NDI_cor_flat %>% mutate(metric="NDI")

knitr::kable(NDI_cor_flat)

#mean correlation coefficient
FisherZInv(mean(NDI_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(NDI_cor_flat$FishersF))


```

# ODI

```{r ODI}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "odiWatWM"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


ODI_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

ODI_cor_flat <- ODI_cor_flat %>% mutate(FishersF = FisherZ(cor))

ODI_cor_flat <- ODI_cor_flat %>% mutate(metric="ODI")

knitr::kable(ODI_cor_flat)

#mean correlation coefficient
FisherZInv(mean(ODI_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(ODI_cor_flat$FishersF))


```

# ISO

```{r ISO}

#ASSIGN THE METRIC VARIABLE NAME HERE 

metric <- "fisoWatWM"

#read in the tract average metric file

tracts_raw <- read.csv(here("data", "processed", "weighted_average_calculated_raw", paste(metric, "tracts_220.csv", sep="_")))

#first set record_id to row names
tracts_raw2 <- tracts_raw %>% remove_rownames %>% column_to_rownames(var="record_id")

cor_matrix <- rcorr(as.matrix(tracts_raw2))

corrplot(cor_matrix$r, type="full", order="hclust", 
         p.mat = cor_matrix$P, sig.level = 0.05, insig = "blank", tl.col = "black", method = "color", tl.srt = 45)


ISO_cor_flat <- flattenCorrMatrix(cor_matrix$r, cor_matrix$P)

ISO_cor_flat <- ISO_cor_flat %>% mutate(FishersF = FisherZ(cor))

ISO_cor_flat <- ISO_cor_flat %>% mutate(metric="ISO")

knitr::kable(ISO_cor_flat)

#mean correlation coefficient
FisherZInv(mean(ISO_cor_flat$FishersF))

#SD of correlation coefficient
FisherZInv(sd(ISO_cor_flat$FishersF))


```

# Correlation density plot

``` {r density_plot}

#first bind the dataframes

all_cor_flat <- bind_rows(FA_cor_flat, MD_cor_flat, AD_cor_flat, RD_cor_flat, NDI_cor_flat, ODI_cor_flat, ISO_cor_flat)

#reorder the metric levels
all_cor_flat$metric <- factor(all_cor_flat$metric, levels = c("FA", "MD", "AD", "RD", "NDI", "ODI", "ISO"))

ggplot(all_cor_flat, aes(x=cor, color=metric, fill=metric)) +
  geom_density(alpha=0.1) +
  scale_color_manual(values=c("#500870", "#CA3989", "#F1A90E", "#00966C", "#A77588", "#2EB1AB", "#BA2913"), name = "") + 
  scale_fill_manual(values=c("#500870", "#CA3989", "#F1A90E", "#00966C", "#A77588", "#2EB1AB", "#BA2913"), name = "") + 
  ylab("Density") + xlab("Pearson's r") +
  scale_x_continuous(breaks=seq(-0.4,1,0.2)) +
  theme_bw(base_size=20)

```