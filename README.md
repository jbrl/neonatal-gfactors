### Neonatal g-factors

Code to reproduce the analyses presented in 

> Kadi Vaher, Paola Galdi, Manuel Blesa Cabez, Gemma Sullivan, David Q Stoye, Alan J Quigley, Michael J Thrippleton, Debby Bogaert, Mark E Bastin, Simon R Cox, James P Boardman (2021). **General factors of white matter microstructure from DTI and NODDI in the developing brain.** _Neuroimage_, 254:119169 [https://doi.org/10.1016/j.neuroimage.2022.119169](https://doi.org/10.1016/j.neuroimage.2022.119169).


Authors: Kadi Vaher (kadi.vaher@ed.ac.uk), Paola Galdi (paola.galdi@gmail.com) and Manuel Blesa (mblesac@gmail.com)


The repository consists of the following folders:
- **permutations/** containing R code and results of the permutation analysis for prediction models
- **scripts/** containing R markdown files illustrating the PCA, CFA and prediction analysis
- **results/** containing output models, figures and html reports
- **tracts/** containing the tract atlas, scripts and instructions for tract extraction

The ENA50 neonatal template is available at: [https://git.ecdf.ed.ac.uk/jbrl/ena](https://git.ecdf.ed.ac.uk/jbrl/ena)
